const express = require('express');
const cors = require("cors");
const links = require("./app/links");
const mongoose = require("mongoose");
const exitHook = require("async-exit-hook");

const app = express();
app.use(express.json());
app.use(cors());

const port = 8000;

app.use("/", links);

const run = async () => {
  await mongoose.connect("mongodb://localhost/link_shortener", {useNewUrlParser: true, useUnifiedTopology: true});

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  exitHook(async callback => {
    await mongoose.disconnect();
    callback();
  });
};

run().catch(console.error);