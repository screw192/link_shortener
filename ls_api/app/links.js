const express = require('express');
const {nanoid} = require('nanoid');
const Link = require("../models/Link");


const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const links = await Link.find();
    res.send(links);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:shortUrl', async (req, res) => {
  try {
    const link = await Link.findOne({shortUrl: req.params.shortUrl});

    if (link) {
      res.status(301).redirect(link["originalUrl"]);
    } else {
      res.sendStatus(404);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/links', async (req, res) => {
  try {
    const linkData = req.body;

    let shortUrlKey;
    let linkMatch;

    do {
      shortUrlKey = nanoid(7);
      linkMatch = await Link.findOne({shortUrl: shortUrlKey});
    } while (linkMatch !== null);

    linkData.shortUrl = shortUrlKey;

    const link = new Link(linkData);
    await link.save();

    res.send(link);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;