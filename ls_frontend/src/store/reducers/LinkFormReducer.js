import {
  ORIGINAL_URL_INPUT_CHANGE,
  POST_TO_SHORTEN_FAILURE,
  POST_TO_SHORTEN_REQUEST,
  POST_TO_SHORTEN_SUCCESS
} from "../actions/LinkFormActions";

const initialState = {
  originalUrl: "",
  shortUrl: "",
};

const linkReducer = (state = initialState, action) => {
  switch (action.type) {
    case ORIGINAL_URL_INPUT_CHANGE:
      return {...state, originalUrl: action.inputValue};
    case POST_TO_SHORTEN_REQUEST:
      return {...state};
    case POST_TO_SHORTEN_SUCCESS:
      return {...state, shortUrl: action.shortLink};
    case POST_TO_SHORTEN_FAILURE:
      return {...state};
    default:
      return state;
  }
};

export default linkReducer;