import axiosApi from "../../axiosApi";

export const ORIGINAL_URL_INPUT_CHANGE = "ORIGINAL_URL_INPUT_CHANGE";

export const POST_TO_SHORTEN_REQUEST = "POST_TO_SHORTEN_REQUEST";
export const POST_TO_SHORTEN_SUCCESS = "POST_TO_SHORTEN_SUCCESS";
export const POST_TO_SHORTEN_FAILURE = "POST_TO_SHORTEN_FAILURE";

export const originalUrlInputChange = inputValue => ({type: ORIGINAL_URL_INPUT_CHANGE, inputValue});

export const postToShortenRequest = () => ({type: POST_TO_SHORTEN_REQUEST});
export const postToShortenSuccess = shortLink => ({type: POST_TO_SHORTEN_SUCCESS, shortLink});
export const postToShortenFailure = () => ({type: POST_TO_SHORTEN_FAILURE});

export const postToShorten = link => {
  return async dispatch => {
    try {
      const data = {originalUrl: link}
      dispatch(postToShortenRequest());

      const response = await axiosApi.post("/links", data);
      dispatch(postToShortenSuccess(response.data.shortUrl));
    } catch (e) {
      dispatch(postToShortenFailure());
    }
  };
};