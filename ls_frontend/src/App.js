import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import makeStyles from  "@material-ui/core/styles/makeStyles";

import LinkForm from "./containers/LinkForm/LinkForm";
import ShortenedLink from "./containers/ShortenedLink/ShortenedLink";


const useStyles = makeStyles({
  mainContainer: {
    height: "100vh",
  }
});

const App = () => {
  const classes = useStyles();

  return (
    <>
      <CssBaseline/>
      <Container maxWidth="lg">
        <Grid
            className={classes.mainContainer}
            container
            direction="column"
            justify="center"
            spacing={5}
        >
          <Grid item>
            <LinkForm/>
          </Grid>
          <Grid item>
            <ShortenedLink/>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default App;