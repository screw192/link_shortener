import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";

import {originalUrlInputChange, postToShorten} from "../../store/actions/LinkFormActions";


const LinkForm = () => {
  const dispatch = useDispatch();
  const originalUrl = useSelector(state => state.linkForm.originalUrl);

  const onUrlInputChange = event => {
    dispatch(originalUrlInputChange(event.target.value));
  };

  const onUrlSubmit = event => {
    event.preventDefault();

    dispatch(postToShorten(originalUrl));
  };

  return (
      <form onSubmit={onUrlSubmit}>
        <Grid
            container
            direction="column"
            spacing={3}>
          <Grid item>
            <Typography align="center" variant="h3">Shorten your link!</Typography>
          </Grid>
          <Grid item>
            <TextField
                fullWidth
                variant="outlined"
                value={originalUrl}
                onChange={onUrlInputChange}
            />
          </Grid>
          <Grid item container justify="center">
            <Grid item>
              <Button
                  variant="contained"
                  color="primary"
                  size="large"
                  type="submit"
              >
              Shorten
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </form>
  );
};

export default LinkForm;