import React from 'react';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import {useSelector} from "react-redux";

import {apiURL} from "../../config";


const ShortenedLink = () => {
  const shortLinkKey = useSelector(state => state.linkForm.shortUrl);
  const shortLink = `${apiURL}/${shortLinkKey}`;

  let shortenedLinkBlock;
  if (shortLinkKey !== "") {
    shortenedLinkBlock = (
        <Grid container direction="column" spacing={2}>
          <Grid item>
            <Typography align="center" variant="h5">
              Your link now looks like this:
            </Typography>
          </Grid>
          <Grid item container justify="center">
            <Grid item>
              <Link variant="h5" href={shortLink}>{shortLink}</Link>
            </Grid>
          </Grid>
        </Grid>
    );
  } else {
    shortenedLinkBlock = null;
  }

  return (
      shortenedLinkBlock
  );
};

export default ShortenedLink;